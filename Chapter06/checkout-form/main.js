/*****************************
 *    EVENT LISTENER SETUP
 *****************************/


// Add any event listener setup you need here.  Eg, you will probably need a window.load handler:

window.addEventListener('load', function() {
    // stuff that needs to happen when the page loads
})

// You will also need others.  Add them here...



/******************************************************************/

// Complete the methods below according to the Lab requirements

// Validation for the card-number text field
function validateCardNumber() {

}

// Validation for the ccv text field
function validateCCV() {

}

// Validation for the expiry-month select field
function validateExpiryMonth() {

}

// Validation for the expiry-year select field
function validateExpiryYear() {

}

// Removes invalid characters and adds spaces (to assist readability) 
// to the card-number text field value
function formatCardNumber() {

}

/**
 * @param cardNumber A card number to analyze
 *
 * @return Returns one of 'visa', 'mastercard', 'amex', or 'other' 
 *         depending on the given cardNumber
 */
function detectCardType(cardNumber) {

}

// Selects the correct card type based on the card number as determined by detectCardType
function autoSelectCardType() {
    
}

// Add expiry year <option>s to the expiry-year <select> field
function addExpiryYears() {
          
}

/*************************************************************************/

// You may add other helper functions below here if you feel the need...